# Badblocks Wrapper scripts by PCO

A collection of simples scripts wrapping most used commands in PCO

## Script list:

* bb_wrap (select a disk and execute a destructive writing/reading cycle)

## Requirements:

* dialog
* bash
* sudo enabled (or root execution)

## Todo:

* check if used program are installed
* unmount all (except live medium / rootfs) option
* report/log save path option
* main menu
* source script for common functions (e.g. select_disk)
* badblocks report to file
* Programs support:
  * smartctl (with report)
  * lshw or similar (with report)