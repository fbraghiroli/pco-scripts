#!/bin/bash

#Ui conf
DIA="dialog"
BACKTITLE="Badblocks Wrapper scripts by PCO"

#BB="ls "
BB="badblocks -v -s -w -t random "
XTERM="x-terminal-emulator -e bash -c"

input=/tmp/menu.sh.$$
trap "rm -f $input; clear" EXIT

echoerr() { echo "$@" 1>&2; }

#Get ourself root (TODO find a best way to do this)
if [ "$(whoami)" != "root" ]; then
    sudo su -s "$0"
    exit
fi

function wait_user
{
    echo
    echo "Press any key to continue"
    read
}

function select_disk
{
    sel_disks=()
    while true;
    do
	DISKS=($(lsblk -r --output NAME,SIZE | awk '{if (NR!=1) {print $1,$2,"0ff"}}'))

	$DIA --clear --backtitle "$BACKTITLE" --separate-output \
	     --checklist \
	     "Please select one or more disk[s] from the following list." \
	     20 70 15 \
	     "${DISKS[@]}" 2>"${input}"
	if [ $? = "0" ]; then
	    in=$(<"${input}")
	    if [ -n "$in" ]; then
	       sel_disks=$in
	       break;
	    fi
	else
	    break;
	fi
    done
}

function select_operation
{
    if [ "$sel_disks" = "" ]; then
	select_disk
    fi

    if [ "$sel_disks" = "" ]; then
	echoerr "No disk selected"
	$DIA --msgbox "At least one disk must be selected." 6 40
	return
    fi

    #TODO: check if previous selection is valid
while true
    do
	menuitem="WRND"
	$DIA --clear --backtitle "$BACKTITLE" \
	     --title "Main Menu" --menu "Choose Operation:" \
	     10 70 20 \
	     WRND "Write and verify for bad blocks" 2>"${input}"
	if [ $? = "0" ]; then
	    menuitem=$(<"${input}")
	    case $menuitem in
		WRND)
		    bb_wrnd
		    break
		    ;;
	    esac
	else
	    clear
	    break
	fi

    done

}

function bb_wrnd
{
    list_d=$(echo ${sel_disks} | sed -e "s/ /\\\n/g")
    $DIA --yesno "The following disks will be overwritten:\n
${list_d}\n\nAre you sure?" 20 70
    confirm=$?

    if [ $confirm != 0 ]; then
	return
    fi

    for choice in $sel_disks
    do
	$XTERM "${BB} /dev/${choice}; read" &
    done
    sel_disks=""
}

while true
do
    menuitem="Disk-Sel"
    $DIA --clear --backtitle "$BACKTITLE" \
	 --title "Main Menu" --menu "Choose Operations:" \
	 10 70 5 \
	 Disk-Sel "Disk[s] select" \
	 Op-Sel "Operation select " 2>"${input}"
    if [ $? = "0" ]; then
	menuitem=$(<"${input}")
	case $menuitem in
	    Disk-Sel)
		select_disk
		;;
	    Op-Sel)
		select_operation
		;;
	esac
    else
	clear
	exit
    fi

done
